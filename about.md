---
layout: blank
---

## Contact

Sonny Kothapally <br> <[me@sonnyksimon.com](mailto:me@sonnyksimon.com)> <br> 58 Brickdam <br> Stabroek, Georgetown, GY <br>

I prefer email. If something is urgent, you should really send it again to me. I don't use the phone. If you have some reason to call me, please warn me first by email.

## Background

**Web:** Apache, Python

**Databases:** Oracle, PostgreSQL

**Languages:** C, Python, Javascript, bash and English

**Operating Systems:** Ubuntu, Debian GNU/Linux

**Study:** computing, thought (ala [Chomsky](http://www.haymarketbooks.org/authors/28-noam-chomsky)), physics (ala [Feynman](http://feynmanlectures.caltech.edu))

## Schooling

(2020) B.S. *Computing and Information Systems*. **University of London**.

(2016) A.S. *Physics, Chemistry, Pure Mathematics, Computer Science, Digital Media*. **Queen’s College, Guyana**.

(2014) Cert. *Sciences*. **Central High, Guyana**.

## Favorite Quotes

"You don't have to be responsible for the world that you're in" - John von Neumann

"I’d rather be hated for who I am, than loved for who I’m not." - K. Cobain

"The optimist thinks this is the best of all possible worlds. The pessimist fears it is true." - J. R. Oppenheimer

"There are two possible outcomes: if the result confirms the hypothesis, then you’ve made a measurement. If the result is contrary to the hypothesis, then you’ve made a discovery." E. Fermi

"If a man has any genuine talent he should be ready to make almost any sacrifice in order to cultivate it to the full." G. H. Hardy

"We are at the very beginning of time for the human race. It is not unreasonable that we grapple with problems. But there are tens of thousands of years in the future. Our responsibility is to do what we can, learn what we can, improve the solutions, and pass them on." - R. P. Feynman

"**If we are honest — and scientists have to be — we must admit that religion is a jumble of false assertions, with no basis in reality. The very idea of God is a product of the human imagination.** It is quite understandable why primitive people, who were so much more exposed to the overpowering forces of nature than we are today, should have personified these forces in fear and trembling. But nowadays, when we understand so many natural processes, we have no need for such solutions. I can’t for the life of me see how the postulate of an Almighty God helps us in any way. What I do see is that this assumption leads to such unproductive questions as why God allows so much misery and injustice, the exploitation of the poor by the rich and all the other horrors He might have prevented. If religion is still being taught, it is by no means because its ideas still convince us, but simply because some of us want to keep the lower classes quiet. Quiet people are much easier to govern than clamorous and dissatisfied ones. They are also much easier to exploit. Religion is a kind of opium that allows a nation to lull itself into wishful dreams and so forget the injustices that are being perpetrated against the people. Hence the close alliance between those two great political forces, the State and the Church. Both need the illusion that a kindly God rewards — in heaven if not on earth — all those who have not risen up against injustice, who have done their duty quietly and uncomplainingly. That is precisely why the honest assertion that God is a mere product of the human imagination is branded as the worst of all mortal sins." - P. A. M. Dirac

## Places I've Been To

**Guyana**: Georgetown; **India**: Bangalore

## Games I've Played

SuperTux, TuxRacer, PepsiMan, Road Rash, DxBall, Pinball, 4dPrinceOfPersia, SkyRoads, Dangerous Dave, Icy Tower, Age of Empires, Praetorians, Pokemon Yellow, Pokemon Yellow, Pokemon Sapphire, FIFA 12, FIFA 15, FIFA 17, NARUTO: Ultimate Ninja STORM, Naruto Shippuden: Ultimate Ninja Storm 2, Naruto Shippuden: Ultimate Ninja Storm 3, and Naruto Ultimate Ninja Storm 4
