---
layout: blank
---

## Websites

[**pancubs.org**](http://pancubs.org)

> Collection of really interesting people, and projects.

[**sonnyksimon.com**](http://sonnyksimon.com)

> This site.

[**bk**](http://bk.sonnyksimon.com)

> A repository of wuxia novels.

[**am.otaku.rip**](http://am.otaku.rip)

> Shows I enjoyed watching.

## Utility Tools

[**caltext**](http://github.com/sonnyksimon/caltext)

> Automatically generates a plaintext calendar between two dates.

[**dbq.py**](http://github.com/sonnyksimon/dbq.py)

> This little Python script runs SQL SELECT statements on databases from the command-line. Now available as an [API](http://dbq.sonnyksimon.com).

[**likepandas.py**](http://github.com/sonnyksimon/likepandas.py)

> likepandas.py is a tool that scrapes Instagram's #pandas page, and likes the latest images they've got. *Update*: I've 
also added comments.

## Other peoples' software

[**FarmersMarket.Gy**](http://farmersmarket.gy)

> A portal that connects farmers with prospective buyers.

[**Community.Gy**](http://community.gy)

> Platform to track incidents in the community and manage property rates and tax.

[**IRISReport.com**](http://irisreport.com)

> A bot for disseminating information to the public about a [disruptive road project](http://web.archive.org/web/20190527180954/http://guyanachronicle.com/2019/04/05/sheriff-street-mandela-avenue-road-project-to-be-accelerated).

[**CoActing.org**](http://coacting.org)

> A platform to share thoughts, and promote civic awareness of social issues.
