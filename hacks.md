---
layout: blank
title: things i'm working on (sonny kothapally)
charset: utf-8
description: Sonny Kothapally is a hacker. He is currently enrolled in a distance learning programme studying computers.
url: http://www.sonnyksimon.com/hacks
locale: en_US
type: website
site_name: sonnyksimon
---

Back to [home](/).

<hr/>

## Things I'm Working On

1.  [coacting.org](http://coacting.org/) ( a platform to share thoughts )
2.  [renpan](http://github.com/pancubs/renpan) ( a panbot that looks up various news sites and filters the good ones (imho) )
3.  [everything else](http://github.com/sonnyksimon) ( source control )

## Websites

[**pancubs.org**](http://pancubs.org)

> Collection of really interesting people, and projects. I haven't started anything yet on this.

[**sonnyksimon.com**](http://sonnyksimon.com)

> Personal website

[**am.otaku.rip**](http://am.otaku.rip)

> shows I watch

## Other Peoples' Software I've Hacked On

[**FarmersMarket.Gy**](http://farmersmarket.gy)

> A portal that connects farmers with prospective buyers.

[**Community.Gy**](http://community.gy)

> Platform to track incidents in the community and manage property rates and tax.

[**IRISReport.com**](http://irisreport.com)

> A bot for disseminating information to the public about a [disruptive road project](http://web.archive.org/web/20190527180954/http://guyanachronicle.com/2019/04/05/sheriff-street-mandela-avenue-road-project-to-be-accelerated).

<hr/>

Back to [home](/).
