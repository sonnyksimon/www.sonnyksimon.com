---
layout: blank
title: what i'm reading (sonny kothapally)
charset: utf-8
description: Sonny Kothapally is a hacker. He is currently enrolled in a distance learning programme studying computers.
url: http://www.sonnyksimon.com/reading
locale: en_US
type: website
site_name: sonnyksimon
---

Back to [home](/).

<hr/>

## What I'm Reading
I seem to spend so much of my time watching television shows lately that I wasn't reading nearly enough books. I suspect I won't make it through but hopefully there's some progress there.

These are some really interesting books I've encountered, and I *have* to recommend these first.

**Noteworthy**: [Perfume](http://en.wikipedia.org/wiki/Perfume_(novel))

**Amusing**: [A Mathematician's Apology](http://en.wikipedia.org/wiki/A_Mathematician%27s_Apology), [The Prince](http://en.wikipedia.org/wiki/The_Prince), [Crito](http://en.wikipedia.org/wiki/Crito)

**Feynman**: [ALL OF THESE](http://en.wikipedia.org/wiki/Richard_Feynman#Popular_works)

**Archaic**: [Nicolai Copernici Torinensis De reuolutionibus orbium coelestium, libri VI](http://en.wikipedia.org/wiki/Nicolaus_Copernicus#The_book)

**Random**: [Coiling Dragon](http://coiling-dragon.fandom.com/wiki/Coiling_Dragon_Wiki) by I Eat Tomatoes (我吃西红柿), [I Shall Seal The Heavens](http://i-shall-seal-the-heavens.fandom.com/wiki/Home) by Er Gen (耳根)

The books I'm currently reading are pretty technical, but here they are categorized (this is also a makeshift to-read list):

*from http://en.tldp.org/HOWTO/Reading-List-HOWTO/b80.html*

> Books on Unix culture
> 
> * The New Hacker's Dictionary, Third Edition, Edited by Eric S. Raymond, 1996, ISBN 0-262-68092-0, MIT Press, 547pp.
> 
> * A Quarter Century of Unix, Edited by Peter H. Salus, 1994, ISBN 0-201-54777-5, Addison-Wesley, 255pp.
> 
> * The Mythical Man Month, Anniversary Edition, Frederic P. Brooks, 1995, ISBN 0-201-83595-9, Addison-Wesley.
> 
> * The Cathedral and the Bazaar, Second Edition, Edited by Eric S. Raymond, 1999, ISBN 0-596-00131-2, O'Reilly & Associates, 240pp.
> 
> Linux basics
> 
> * Linux System Administrator's Guide, Edited by Lars Wirzenius, 1997, Linux Documentation Project.
> 
> * Linux in a Nutshell, Fourth Edition, Ellen Siever, Stephen Figgins, Aaron Weber, 2003, ISBN 0-596-00482-6, O'Reilly & Associates.
> 
> * Running Linux, Fourth Edition, Matt Welsh, Matthias Dallheimer, Terry Dawson, Lar Kaufman, 2002, 0-596-00272-6, O'Reilly & Associates.
> 
> * A Practical Guide to Linux, Mark G. Sobell, 1998, ISBN 0-201-89549-8, Addison-Wesley, 1072pp.
> 
> * Essential System Administration, 3rd Edition, Æleen Frisch, 2002, ISBN 0-596-00343-9, O'Reilly & Associates.
> 
> System security
> 
> * Security Engineering: A Guide to Building Dependable Distributed Systems, Ross Anderson, 2001, 0-471-38922-6, Wiley.
> 
> * Real World Linux Security: Intrusion Prevention, Detection, and Recovery, 2nd edition, Bob Toxen, 2003, ISBN 0-13-046456-2, Prentice-Hall.
> 
> Shell, script and web programming
> 
> * Programming Perl, Third Edition, Larry Wall, Tom Christiansen, and Jon Orwant, 2000, ISBN 0-596-00027-8, O'Reilly & Associates, 1104pp.
> 
> * Programming Python, Second Edition, Mark Lutz, 2001, ISBN 0-596-00085-5, O'Reilly & Associates.
> 
> * HTML & XHTML: The Definitive Guide, Fifth Edition, Chuck Musciano and Bill Kennedy, 2002, ISBN 0-596-00382-X, O'Reilly & Associates, 680pp.
> 
> * The Unix Programming Environment, Brian Kernighan and Rob Pike, 1984, ISBN 0-13-937681-X, Prentice-Hall.
> 
> TeX and LaTeX
> 
> * The LaTeX Companion, Michael Goossens, Frank Mittelbach, and Alexander Samarin, 1994, ISBN 0-201-54199-8, Addison-Wesley, 530pp.
> 
> * LaTeX: A Document Preparation System, Leslie Lamport, 1994, ISBN 0-201-52983-1, Addison-Wesley, 256pp.
> 
> * The TeXbook, Volume A of Computers and Typesetting, Donald Knuth, 1986, ISBN 0-201-13448-9, Addison-Wesley, 496pp.
> 
> * The METAFONT Book, Volume C of Computers and Typesetting, Donald Knuth, 1986, ISBN 0-201-13444-6, Addison-Wesley, 386pp.
> 
> Good programming style
> 
> * The Practice of Programming, Brian Kernighan and Rob Pike, 1999, ISBN 0-201-61586-X, Addison-Wesley.
> 
> * Programming Pearls, (Second Edition), Jon Bentley, 2000, ISBN 0-201-65788-0, Addison-Wesley.
> 
> * The Art of Unix Programming, Edited by Eric S. Raymond, 2003, ISBN 0-131-42901-9, Addison-Wesley, 512pp.
> 
> * Code Reading: The Open Source Perspective, Edited by Diomedis Spinellis, 2003, Addison-Wesley.
> 
> * Writing Efficient Programs, Jon Bentley, 1982, ISBN 0-13-970251-2 or 0-13-970244-X, Prentice-Hall.
> 
> C and C++
> 
> * The C Programming Language, (Second Edition), Brian Kernighan and Dennis Ritchie, 1988, ISBN 0-13-110362-8, Addison-Wesley, 272pp.
> 
> * Who's Afraid of C++?, Steve Heller, 1996, ISBN 0-12-339097-4, Academic Press, 508pp.
> 
> C system call interface
> 
> * POSIX Programmer's Guide: Writing Portable Unix Programs, Donald Lewine, 1992, ISBN 0-937175-73-0, O'Reilly & Associates, 607pp.
> 
> * Advanced Programming in the Unix Environment, W. Richard Stevens, 1993, ISBN 0-201-56317-7, Addison-Wesley.
> 
> * Linux Application Development, Michael K. Johnson and Erik W. Troan, 1998, ISBN 0-201-308215, Addison-Wesley.
> 
> Books on networking
> 
> * Unix Network Programming, volume 1 -- Networking APIs: Sockets and XTI, W. Richard Stevens, 1998, ISBN 0-13-490012-X, Prentice-Hall.
> 
> * Unix Network Programming, volume 2 -- Interprocess Communication, Richard Stevens, 1998, ISBN 0-13-081081-9, Prentice-Hall.
> 
> * Linux Network Administrator's Guide, Olaf Kirch, 1995, ISBN 1-56592-087-2, O'Reilly & Associates.
> 
> * TCP/IP Network Administration, Craig Hunt, 1992, ISBN 0-937175-82-X, O'Reilly & Associates, 472pp.
> 
> * DNS and BIND, Second Edition, Paul Albiz and Cricket Liu, 1998, ISBN 1-56592-512-2, 502pp., O'Reilly & Associates.
> 
> * Sendmail, Third Edition, Bryan Costales and Eric Allman, 2002, ISBN 1-56592-839-3, 1232pp., O'Reilly & Associates.
> 
> Ancestors of Linux
> 
> * The Design of the Unix Operating System, Maurice J. Bach, 1996, ISBN 0-13-201799-7, 470pp., Prentice-Hall.
> 
> * Operating Systems, Design and Implementation, Andrew S. Tanenbaum, 1987, ISBN 0-13-638677-6, 940pp., Prentice-Hall.
> 
> The Linux kernel
> 
> * The Linux Kernel book, Rémy Card, Èric Dumas, and Frank Mével, 1998, ISBN 0-471-98141-9, John Wiley & Sons.
> 
> * LINUX Kernel Programming, (Third Edition), Michael Beck, Harold Bohme, Mirko Dziadka, Robert Magnus, Claus Schroter, and Dirk Verworner, 2002, ISBN 0-201-719754, Addison-Wesley, 480pp.
> 
> Relatives of Linux
> 
> * The Design and Implementation of the 4.4BSD Unix Operating System, Marshall Kirk McKusick, Keith Bostic, Michael J. Karels, and John S. Quarterman, 1996, ISBN 0-201-54979-4, Addison-Wesley.
> 
> Books on Intel and PC hacking
> 
> * 80386 Programmer's Reference Manual, Intel Corporation, 1986, ISBN 1-55512-022-9.
> 
> * 80386 System Software Writer's Guide, Intel Corporation, 1987, ISBN 1-55512-023-7.
> 
> * Programming the 80386, John H. Crawford and Patrick P. Gelsinger, 1987, ISBN 0-89588-381-3, 774pp.
> 
> * 80386 Hardware Reference Manual, Intel Corporation, 1986, ISBN 1-55512-024-5.
> 
> * The Indispensable PC Hardware Book, Hans-Peter Messmer, 1993, ISBN 0-201-62424-9, 1000pp., Addison-Wesley.

*from https://github.com/ana2bell/awesome-python-books*

> Algorithms
> 
> * Python Algorithms: Mastering Basic Algorithms in the Python Language by Magnus Lie Hetland
> 
> Complexity Science
> 
> * Think Complexity: Complexity Science and Computational Modeling by Allen B. Downey
> 
> Game Development
> 
> * Making Games with Python & Pygame by Al Sweigart
> 
> Hardware
> 
> * Real World Instrumentation with Python: Automated Data Acquisition and Control Systems by John M. Hughes
> 
> Machine Learning
> 
> * Building Machine Learning Systems with Python by Luis Pedro Coelho & Willi Richert
> 
> * Machine Learning in Action by Peter Harrington
> 
> * Programming Collective Intelligence: Building Smart Web 2.0 Applications by Toby Segaran
> 
> * Python Machine Learning by Sebastian Raschka
> 
> Natural Language Processing
> 
> * Mastering Python Regular Expressions by Felix Lopez & Victor Romero
> 
> * Natural Language Processing with Python by Steven Bird & Ewan Klein & Edward Loper
> 
> Network
> 
> * Python Network Programming Cookbook by Dr. M. O. Faruque Sarker
> 
> Science and Data Analysis
> 
> * Data Science From Scratch: First Principles with Python by Joel Grus
> 
> * Python for Data Analysis: Data Wrangling with Pandas, NumPy, and IPython by Wes McKinney
> 
> * SciPy and NumPy: An Overview for Developers by Eli Bressert
> 
> * NumPy Beginner's Guide by Ivan Idris
> 
> * Python for Finance: Analyze Big Financial Data by Yves Hilpisch
> 
> * A Programmer's Guide to Data Mining by Ron Zacharski
> 
> Security
> 
> * Black Hat Python: Python Programming for Hackers and Pentesters by Justin Seitz
> 
> * Gray Hat Python: Python Programming for Hackers and Reverse Engineers by Justin Seitz
> 
> * Violent Python: A Cookbook for Hackers, Forensic Analysts, Penetration Testers and Security Engineers by TJ O'Connor
> 
> SQL and ORMs
> 
> * Essential SQLAlchemy by Jason Myers
> 
> System Administration
> 
> * Python for Unix and Linux System Administration by Noah Gift & Jeremy Jones
> 
> Testing
> 
> * Testing Python: Applying Unit Testing, TDD, BDD and Acceptance Testing by David Sale
> 
> Web Development
> 
> * Flask Web Development: Developing Web Applications with Python by Miguel Grinberg
> 
> * Mastering Flask by Jack Stouffer
> 
> * Introduction to Tornado by Michael Dory & Allison Parrish & Brendan Berg
> 
> * Test-Driven Development with Django by Kevin Harvey
> 
> * Test-Driven Development with Python by Harry J. W. Percival
> 
> Web Scraping
> 
> * Web Scraping with Python: A Comprehensive Guide to Data Collection Solutions by Ryan Mitchell English Edition

<hr/>

Back to [home](/).
