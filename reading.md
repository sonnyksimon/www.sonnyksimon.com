---
layout: blank
---

## What I'm Reading

These are some really interesting books I've encountered that I *have* to recommend.

[Perfume: The Story of a Murderer](http://en.wikipedia.org/wiki/Perfume_(novel))

[A Mathematician's Apology](http://en.wikipedia.org/wiki/A_Mathematician%27s_Apology), [The Prince](http://en.wikipedia.org/wiki/The_Prince), [Crito](http://en.wikipedia.org/wiki/Crito)

[Feynman's works](http://en.wikipedia.org/wiki/Richard_Feynman#Popular_works)

[De reuolutionibus orbium coelestium](http://en.wikipedia.org/wiki/Nicolaus_Copernicus#The_book)

[Coiling Dragon](http://coiling-dragon.fandom.com/wiki/Coiling_Dragon_Wiki), [I Shall Seal The Heavens](http://i-shall-seal-the-heavens.fandom.com/wiki/Home)
