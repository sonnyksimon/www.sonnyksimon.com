---
layout: post
title: What is going on here?
---
I want to use this as my thinking space, not doing this for someone else but for me.

The idea's not mine: both the theme and the idea are from [this post](http://www.aaronsw.com/weblog/about).
