---
layout: post
charset: utf-8
description: Sonny Kothapally is a hacker. He is currently enrolled in a distance learning programme studying computers.
url: http://www.sonnyksimon.com/
locale: en_US
type: website
site_name: sonnyksimon
permalink: /weblog/:title
category: thought
---
Another post after nearly half a decade::

Information is stored a bit weird, especially in computers.

There are apps which are made up of stuff and every stuff has some memory and info.

Memory is things like where(ip?) and when(unix_timestamp?) data was created (not really, just moving bits around).

Pieces of information make up the core of stuff.

Each piece of information also persists for a while (sometimes also for an instant) in memory.

It’s pretty weird see: [here’s a sketch instead](/db.png).
