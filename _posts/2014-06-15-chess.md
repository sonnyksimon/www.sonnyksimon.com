---
layout: post
charset: utf-8
description: Sonny Kothapally is a hacker. He is currently enrolled in a distance learning programme studying computers.
url: http://www.sonnyksimon.com/
locale: en_US
type: website
site_name: sonnyksimon
permalink: /weblog/:title
category: thought
---
How is chess similar to life? It’s simple. Many do not know how to play chess.

Life in it’s essence brings all sorts of trials in our paths and our decisions are not necessarily the right ones. In fact, there may be more than one strategy to beat this cruel game. But some of us cannot see it because we do not understand this game as it is. We complain, get angry, and try to blame it on something other than us. However, there does exist a way to win though only some reach it.

To win, you have to take risks.

Just like in chess, without risking anything; you cannot achieve anything worth of value. However, the pain of losing it may be great but your eyes should be on the prize. This does not mean that you WILL lose it. It only means that you are risking it. And don’t forget that there is a chance you may not be able to bring back what you lost. But if you give up now, you will suffer utter defeat.

See through life and what it has to offer. Play your choices wisely, every turn counts. Don’t lose even one opportunity to better your chances. Think, think hard. You will need it. Sometimes, you also have to be bold to confront the enemy head-on.

Most importantly, do not consider yourself the king. Chess is similar to life but it is not life. In life, the goal is not to defeat someone’s king but rather for you to become the king. You will never be the king unless you are the best in the world. So keep fighting. And you may achieve it. For though how small the chance, it still exists. A way to beat the game. A way to win life.

Do not forget: most of us can’t play chess, but there are ones who can. Watch out for them. Don’t let them beat you. You don’t need to deceive to win. You need to beat them. Without any tricks. You need to show them the difference in skill. Even if you are weak, they can’t see what you see. Being weak will give you an advantage. Don’t rush it. Take your time. And hone your skills at this life.

Don’t take any shortcuts. The way to beat the game is harder than the game itself. All which I said may not apply to you. You may not have the potential to be the king. But if you do, don’t squander the chances. And know this for a fact.

No one will let you be the best. Beat them until they can’t argue. To rule is the only way for you to be accepted. Whether you rule or are ruled depends on you.

Don’t show mercy.
