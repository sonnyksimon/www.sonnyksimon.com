---
layout: blank
title: interesting-pages (sonny kothapally)
charset: utf-8
description: Sonny Kothapally is a hacker. He is currently enrolled in a distance learning programme studying computers.
url: http://www.sonnyksimon.com/interesting-pages
locale: en_US
type: website
site_name: sonnyksimon
---

Back to [home](/).

<hr/>

## Interesting Pages

1. [Feynman Lectures](http://www.feynmanlectures.caltech.edu/)
2. [Travel hacks](http://matt.might.net/articles/travel-hacks/)
3. [Military Standard, Standard general requirements for electronic equipment](http://everyspec.com/MIL-STD/MIL-STD-0300-0499/MIL-STD-454N_9160/)
4. [Teach yourself programming in 10 years.](http://norvig.com/21-days.html)
5. [HTML Hell](http://catb.org/~esr/html-hell.html)
6. [Things every hacker once knew](http://www.catb.org/~esr/faqs/things-every-hacker-once-knew/)
7. [Undergraduation](http://www.paulgraham.com/college.html)
8. [What every cs major should know](http://matt.might.net/articles/what-cs-majors-should-know/)

<hr/>

Back to [home](/).