---
layout: blank
---

## Who are you, and what do you do?

I'm [Sonny Kothapally](http://sonnyksimon.com). I write stuff, in code for the most part and in prose otherwise. 

## What hardware do you use?

I bought a [Lenovo IdeaPad 330](http://en.wikipedia.org/wiki/IdeaPad#IdeaPad_300_Series), the cheapest Lenovo I could find which still had decent build quality. Lenovos seem the best ones to get if you're not going to go for a MacBook. Data is scattered over multiple SD cards and hard drives, with huge overlaps. I like to keep multiple copies just in case something starts failing.

My laptop runs GNU/Linux, and Windows (surf dual booting).

My main mobile device is an [Xperia X Compact](http://en.wikipedia.org/wiki/Sony_Xperia_X_Compact). I use it to keep in touch with friends and family, and listen to [music](http://nirvana.com).

## And what software?

I try to have a limited number of stuff I need:

Web surfing ([w3m](http://w3m.sourceforge.net/)/[Google Chrome](http://google.com/chrome/))

Writing ([Vim](http://www.vim.org/))

Command line ([GNOME Terminal](http://help.gnome.org/users/gnome-terminal/stable/))

All my writing is done in plaintext, and whichever programming language I'm working with.

Everything I'm working on stays in a folder called "Development". I don't like nesting that much, so I just download and keep my programs each contained in a single folder (which is also checked into git). I keep scripts that build my command-line, and install various programs in my [dotfiles repository](http://github.com/sonnyksimon/dotfiles). 

When I need to work on something, I open a new screen window and detach from the session when I'm done. I've read that doing this eats your RAM and that tmux is a better alternative. 
