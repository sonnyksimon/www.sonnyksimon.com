---
layout: blank
title: what i use (sonny kothapally)
charset: utf-8
description: Sonny Kothapally is a hacker. He is currently enrolled in a distance learning programme studying computers.
url: http://www.sonnyksimon.com/usesthis
locale: en_US
type: website
site_name: sonnyksimon
---

Back to [home](/).

<hr/>

A (bad) attempt to tell you what I use to get my job done. It's written in second person.

## Who are you, and what do you do?

I'm [Sonny Kothapally](http://sonnyksimon.com). I write stuff, in code for the most part and in prose otherwise. Actually, you can learn a LOT more about me [here](/about).

## What hardware do you use?

I bought a [Lenovo IdeaPad 330](http://en.wikipedia.org/wiki/IdeaPad#IdeaPad_300_Series), the cheapest Lenovo I could find which still had decent build quality. Lenovos seem the best ones to get if you're not going to go for a MacBook. Data is scattered over multiple SD cards and hard drives, with huge overlaps. I like to keep multiple copies just in case something starts failing.

My laptop runs GNU/Linux.

My main mobile device is an [Xperia X Compact](http://en.wikipedia.org/wiki/Sony_Xperia_X_Compact). I use it to keep in touch with friends and family, and listen to [music](http://nirvana.com).

## And what software?

I try to have a limited number of stuff. In the dock, I keep:

1. [Google Chrome](http://google.com/chrome) (web surfing)
2. [VSCode](http://code.visualstudio.com/) (writing)
3. [Terminal](http://help.gnome.org/users/gnome-terminal/stable/)

All my writing is done in Markdown. I'm trying to switch to ReStructuredText or AsciiDoc but for now that's that.

I mainly program in [JavaScript](https://en.wikipedia.org/wiki/JavaScript). I tend to use [PostgreSQL](http://www.postgresql.org) for my database. I manage revisions with Git. On my servers, I use [GNU screen](http://www.gnu.org/software/screen/screen.html) to manage tasks.

## So how do you actually work?

Everything I'm working on stays in a folder called "Development". I don't like nesting that much, so I just download and keep my programs each contained in a single folder (which is also checked into git). I keep scripts that build my command-line, and install various programs in my [dotfiles repository](http://github.com/sonnyksimon/dotfiles). 

When I need to work on something, I open a new screen window and detach when I'm done. I've read that doing this eats your RAM and using tmux is a better alternative. But I'm always only working on a small number of things at a time, so it works out somehow.

<hr/>

Back to [home](/).
