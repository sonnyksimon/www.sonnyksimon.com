---
layout: blank
title: sonny kothapally
charset: utf-8
description: Sonny Kothapally is a hacker. He is currently enrolled in a distance learning programme studying computers.
url: http://www.sonnyksimon.com/
locale: en_US
type: website
site_name: sonnyksimon
---

# Sonny Kothapally

[weblog](/weblog/) &middot; [twitter](http://twitter.com/sonnyksimon) &middot; [facebook](http://facebook.com/sonny.kothapally) &middot; [flickr](http://flickr.com/sonnyksimon) &middot; [instagram](http://instagram.com/sonnyksimon) &middot; [reddit](http://reddit.com/u/sonnyksimon) &middot; [skype](skype:sonnyksimon?userinfo) &middot; [youtube](http://youtube.com/sonnyksimon)


**Nearby:** [what i read](/reading), [things i'm working on](/hacks), [my schedule](http://calendar.google.com/calendar/embed?src=sonnyksimon@gmail.com)

**Also close by:** [about me](/about), [good words](/about#quotes), [interesting pages](/interesting-pages), [what i use](/usesthis)

Sonny Kothapally is a [hacker](http://www.catb.org/esr/faqs/hacker-howto.html) and [daoist](http://www.iep.utm.edu/daoism/#H5) on the web. He is currently enrolled in a [distance learning programme](http://london.ac.uk/courses/computing-and-information-systems) studying computers.

> *Hacking is done on open source.*
>
> *Hacking is lightweight and exploratory.*
>
> *Hacking places a high value on modularity and reuse.*
>
> *Hacking favors scrap-and-rebuild over patch-and-extend.*
>
> (excerpt taken from [here](http://www.catb.org/esr/faqs/hacking-howto.html))

<[me@sonnyksimon.com](mailto:me@sonnyksimon.com)>

*Original articles on this site are CC [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode) licensed unless otherwise stated.*
{: style="font-size: 0.7em"}

*last updated June 2019*
{: style="font-size: smaller"}
