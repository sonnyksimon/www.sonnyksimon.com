---
layout: blank
title: "Sonny S. Kothapally"
---

# Sonny S. Kothapally

**Distant**: [twitter](http://twitter.com/sonnyksimon), [facebook](http://facebook.com/sonny.kothapally), [flickr](http://flickr.com/sonnyksimon), [instagram](http://instagram.com/sonnyksimon), [reddit](http://reddit.com/u/sonnyksimon), [skype](skype:sonnyksimon?userinfo), [youtube](http://youtube.com/sonnyksiwhat i usemon), [calendar](http://calendar.google.com/calendar/embed?src=sonnyksimon@gmail.com)

**Nearby:** [what i'm reading](/reading), [hacks](/hacks)

**Also close by:** [about me](/about), [interesting pages](/interesting-pages), [what do i do](/usesthis)


<[me@sonnyksimon.com](mailto:me@sonnyksimon.com)>

*Original articles on this site are CC [BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode) licensed unless otherwise stated.*

*last updated November 2019*
